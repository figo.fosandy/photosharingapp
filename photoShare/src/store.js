import {applyMiddleware,createStore,compose} from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './reducers'
import {createLogger} from 'redux-logger'
import {persistStore,persistReducer} from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage'

const persistConfig={
    key:'root',
    storage:AsyncStorage,
    timeout:0,
    blacklist:[]
}

const persistedReducer=persistReducer(persistConfig,rootReducer)

const middlewares=[thunk]
middlewares.push(createLogger())

const store=createStore(
    persistedReducer,
    compose(
        applyMiddleware(...middlewares)
    )
)

let persistor=persistStore(store)

export {store,persistor}