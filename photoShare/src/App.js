import React,{Component} from 'react'
import Navigator from './components/Navigator'
import {persistor,store} from './store'
import {PersistGate} from 'redux-persist/integration/react'
import {Provider} from 'react-redux'

export default class App extends Component{
    render(){
        return(
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <Navigator/>
                </PersistGate>
            </Provider>
        )
    }
}