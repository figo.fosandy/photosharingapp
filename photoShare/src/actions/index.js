import {LOGIN_FAILURE,LOGIN_STARTED,LOGIN_SUCCESS,
    UPDATE_PROFILE_FAILURE,UPDATE_PROFILE_SUCCESS,
    UPLOAD_IMAGE_FAILURE,UPLOAD_IMAGE_SUCCESS,
    CHECK_UNIQUE_HANDLE_FAILURE,CHECK_UNIQUE_HANDLE_SUCCESS,
    CHECK_FEED_STARTED,CHECK_FEED_SUCCESS,CHECK_FEED_FAILURE,
    GET_USERS_SUCCESS,GET_USERS_FAILURE,
    FOLLOW_STARTED,FOLLOW_SUCCESS,FOLLOW_FAILURE,
    LOGOUT,
    LIKE,
    COMMENT
} from './types'
import axios from 'axios'
import path from 'path'
import FormData from 'form-data'
import config from '../../config.json'

const like=(payload)=>{
    return {
        type:LIKE,
        payload
    }
}

const newComment=(payload)=>{
    return {
        type:COMMENT,
        payload
    }
}

const logout=()=>{
    return {
        type:LOGOUT
    }
}

const loginStarted=()=>{
    return {
        type:LOGIN_STARTED
    }
}

const loginSuccess=(userDetail)=>{
    return {
        type:LOGIN_SUCCESS,
        payload:userDetail
    }
}

const loginFailure=(error)=>{
    return {
        type:LOGIN_FAILURE,
        payload:error
    }
}

const updateProfileSuccess=(userDetail)=>{
    return {
        type:UPDATE_PROFILE_SUCCESS,
        payload:userDetail
    }
}

const updateProfileFailure=(err)=>{
    return {
        type:UPDATE_PROFILE_FAILURE,
        payload:err
    }
}

const checkUniqueHandleSuccess=(payload)=>{
    return {
        type:CHECK_UNIQUE_HANDLE_SUCCESS,
        payload
    }
}

const checkUniqueHandleFailure=err=>{
    return {
        type:CHECK_UNIQUE_HANDLE_FAILURE,
        payload:err
    }
}

const uploadImageSuccess=(image)=>{
    return {
        type:UPLOAD_IMAGE_SUCCESS,
        payload:image
    }
}

const uploadImageFailure=(err)=>{
    return {
        type:UPLOAD_IMAGE_FAILURE,
        payload:err
    }
}

const checkFeedStarted=()=>{
    return {
        type:CHECK_FEED_STARTED
    }
}

const checkFeedSuccess=(feeds)=>{
    return {
        type:CHECK_FEED_SUCCESS,
        payload:feeds
    }
}

const checkFeedFailre=(error)=>{
    return {
        type:CHECK_FEED_FAILURE,
        payload:error
    }
}

const getUsersSuccess=(list)=>{
    return {
        type:GET_USERS_SUCCESS,
        payload:list
    }
}

const getUsersFailure=(error)=>{
    return {
        type:GET_USERS_FAILURE,
        payload:error
    }
}


const followStarted=()=>{
    return {
        type:FOLLOW_STARTED
    }
}

const followSuccess=(data)=>{
    return {
        type:FOLLOW_SUCCESS,
        payload:data
    }
}

const followFailure=(err)=>{
    return {
        type:FOLLOW_FAILURE,
        payload:err
    }
}

const loginUser=(action,payload)=>{
    return dispatch=>{
        dispatch(loginStarted())
        return axios
        .post(`${config.development.server}/user/${action}`,payload,{
            auth:{
                username:'user',
                password:'password'
            }
        })
        .then(res=>{
            const {data}=res
            dispatch(loginSuccess(data))
            return data
        })
        .catch(err=>{
            dispatch(loginFailure(err))
            return err
        })
    }
}

const checkFeed=(emails)=>{
    return dispatch=>{
        dispatch(checkFeedStarted())
        axios
            .get(`${config.development.server}/user/feed/[${emails}]`,{
                auth:{
                    username:'user',
                    password:'password'
                }
            })
            .then(res=>{
                const {data}=res
                dispatch(checkFeedSuccess(data))
            })
            .catch(err=>{
                dispatch(checkFeedFailre(err))
            })
    }
}
const updateProfile=(email,payload)=>{
    return dispatch=>{
        return axios
        .put(`${config.development.server}/user/profile/${email}`,payload,{
            auth:{
                username:'user',
                password:'password'
            }
        })
        .then(res=>{
            const {data}=res
            dispatch(updateProfileSuccess(data))
            return data
        })
        .catch(err=>{
            dispatch(updateProfileFailure(err))
            return err
        })
    }
}

const uploadImage=(email,file,fileName,caption,isDp)=>{
    const form=new FormData()
    const fileDetail=path.parse(fileName)
    form.append(`image`,{
        uri:file,
        name:fileDetail.base,
        type:`image/${fileDetail.ext.slice(1)}`
    })
    return dispatch=>{
        return axios
        .post(`${config.development.server}/user/image/${email}/${fileName}/${caption}/${isDp}`,form,{
            auth:{
                username:'user',
                password:'password'
            }
        })
        .then(res=>{
            const {data}=res
            dispatch(uploadImageSuccess(data))
            return data
        })
        .catch(err=>{
            dispatch(uploadImageFailure(err))
            return err
        })
    }
}

const checkUniqueHandle=(uniqueHandle)=>{
    return dispatch=>{
        return axios
            .get(`${config.development.server}/user/handle/${uniqueHandle}`,{
                auth:{
                    username:'user',
                    password:'password'
                }
            })
            .then(res=>{
                const {data}=res
                dispatch(checkUniqueHandleSuccess(data))
                return data
            })
            .catch(err=>{
                dispatch(checkUniqueHandleFailure(err))
                return err
            })
    }
}

const getUserList=()=>{
    return dispatch=>{
        axios
            .get(`${config.development.server}/user/all`,{
                auth:{
                    username:'user',
                    password:'password'
                }
            })
            .then(res=>{
                const {data}=res
                dispatch(getUsersSuccess(data))
            })
            .catch(err=>{
                dispatch(getUsersFailure(err))
            })
    }
}

const followFriends=(follower,toBeFollow)=>{
    return dispatch=>{
        dispatch(followStarted())
        axios
            .patch(`${config.development.server}/user/follow`,{follower,toBeFollow},{
                auth:{
                    username:'user',
                    password:'password'
                }
            })
            .then(res=>{
                dispatch(followSuccess(res.data))
            })
            .catch(err=>{
                dispatch(followFailure(err))
            })
    }
}

const likeFeed=(name,email)=>{
    return dispatch=>{
        axios
            .patch(`${config.development.server}/feed/like`,{name,email},{
                auth:{
                    username:'user',
                    password:'password'
                }
            })
            .then(res=>{
                dispatch(like(res.data))
            })
    }
}

const commentFeed=(name,comment)=>{
    return dispatch=>{
        axios
            .patch(`${config.development.server}/feed/comment`,{name,comment},{
                auth:{
                    username:'user',
                    password:'password'
                }
            })
            .then(res=>{
                dispatch(newComment(res.data))
            })
    }
}

export {loginUser,updateProfile,uploadImage,checkUniqueHandle,checkFeed,getUserList,followFriends,logout,likeFeed,commentFeed}