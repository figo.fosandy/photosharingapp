import {CHECK_FEED_STARTED,CHECK_FEED_FAILURE,CHECK_FEED_SUCCESS,
    LOGOUT,
    LIKE,
    COMMENT
} from '../actions/types'

const initialState={
    loading:false,
    list:[],
    error:null
}

export const feedReducer=(state=initialState,action)=>{
    switch(action.type){
        case CHECK_FEED_STARTED:
            return{
                ...state,
                loading:true
            }
        case CHECK_FEED_SUCCESS:
            return{
                ...state,
                list:action.payload,
                loading:false,
                error:null
            }
        case CHECK_FEED_FAILURE:
            return{
                ...state,
                list:[],
                error:action.payload,
                loading:false
            }        
        case LOGOUT:
            return{
                ...initialState
            }
        case LIKE:
            return{
                ...state,
                list:state.list.map(feed=>{
                    if(feed.name==action.payload.name){
                        return action.payload
                    }
                    return feed
                })
            }
        case COMMENT:
            return{
                ...state,
                list:state.list.map(feed=>{
                    if(feed.name==action.payload.name){
                        return action.payload
                    }
                    return feed
                })
            }
        default:
            return state
    }
}