import {combineReducers} from 'redux'
import {loginReducer} from './loginReducer'
import {feedReducer} from './feedReducer'

const appReducers=combineReducers({
    login:loginReducer,
    feed:feedReducer
})

const rootReducer=(state,action)=>{
    return appReducers(state,action)
}

export default rootReducer