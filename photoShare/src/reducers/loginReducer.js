import {LOGIN_FAILURE,LOGIN_STARTED,LOGIN_SUCCESS,
    UPDATE_PROFILE_FAILURE,UPDATE_PROFILE_SUCCESS,
    UPLOAD_IMAGE_SUCCESS,UPLOAD_IMAGE_FAILURE,
    CHECK_UNIQUE_HANDLE_FAILURE,CHECK_UNIQUE_HANDLE_SUCCESS,
    GET_USERS_SUCCESS,GET_USERS_FAILURE,
    FOLLOW_STARTED,FOLLOW_SUCCESS,FOLLOW_FAILURE,
    LOGOUT
} from '../actions/types'

const initialState={
    loading:false,
    userDetail:null,
    error:null,
    isLogin:false,
    userList:[]
}

export const loginReducer=(state=initialState,action)=>{
    switch(action.type){
        case LOGIN_STARTED:
            return{
                ...state,
                loading:true
            }
        case LOGIN_SUCCESS:
            return{
                ...state,
                userDetail:action.payload,
                error:null,
                loading:false,
                isLogin:true
            }
        case LOGIN_FAILURE:
            return{
                ...state,
                userDetail:null,
                error:action.payload,
                loading:false
            }        
        case UPDATE_PROFILE_SUCCESS:
            return{
                ...state,
                userDetail:action.payload,
                errorUpdate:null
            }
        case UPDATE_PROFILE_FAILURE:
            return{
                ...state,
                errorUpdate:action.payload
            }
        case UPLOAD_IMAGE_SUCCESS:
            return{
                ...state,
                userDetail:{
                    ...state.userDetail,
                    images:[
                        ...state.userDetail.images,
                        action.payload
                    ]
                }
            }
        case UPLOAD_IMAGE_FAILURE:
            return{
                ...state,
                errorUpload:action.payload
            }
        case CHECK_UNIQUE_HANDLE_SUCCESS:
            return{
                ...state,
                uniqueHandleTaken:!action.payload.match('available')?true:false
            }
        case CHECK_UNIQUE_HANDLE_FAILURE:
            return{
                ...state,
                checkUniqueHandleError:action.payload
            }
        case GET_USERS_SUCCESS:
            return{
                ...state,
                userList:action.payload
            }
        case GET_USERS_FAILURE:
            return{
                ...state,
                userListError:action.payload
            }
        case FOLLOW_STARTED:
            return{
                ...state,
                followLoading:true,
                followError:null
            }
        case FOLLOW_SUCCESS:
            return{
                ...state,
                followLoading:false,
                followError:null,
                userDetail:action.payload
            }
        case FOLLOW_FAILURE:
            return{
                ...state,
                followLoading:false,
                followError:action.payload
            }
        case LOGOUT:
            return{
                ...initialState
            }
        default:
            return state
    }
}