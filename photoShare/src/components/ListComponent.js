import React,{Component} from 'react'
import {Image,Text,View,TouchableOpacity,TextInput, FlatList} from 'react-native'
import {styles} from '../styles'
import config from '../../config.json'
import Icon from 'react-native-vector-icons/SimpleLineIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import path from 'path'

const commentList=(props,list)=>{
    const detailUser=list.filter(feed=>feed.email==props.email)[0]
    return (
        <>
            <View style={[styles.footer,styles.centerAlignItem,styles.threeMarginBottom]}>
                <Image source={{uri:`${config.development.server}/images/${detailUser.email}/${detailUser.displayPicture}`}}
                    style={[styles.dpMini]}
                    resizeMode='cover'
                />
                <Text style={[styles.baseText,styles.link,styles.nameText]}> {detailUser.firstName} {detailUser.lastName}</Text>
            </View>
            <TouchableOpacity
                disabled={true}
                style={[styles.textInput,styles.touchable,styles.disabledButton,styles.profileContainer,styles.oneHdrdWidth,styles.commentDetailContainer]}
            >
                <Text style={[styles.baseText,styles.link]}>{props.detail}</Text>
            </TouchableOpacity>
        </>
    )
}

export const List=(props,comp)=>{
    const user=comp.props.login.userDetail.email
    const list=comp.props.login.userList
    const detailUser=list.filter(feed=>feed.email==props.email)[0]
    const detail=path.parse(props.name);
    if(comp.state[`${detail.name}Comments`]==undefined){
        comp.setState({[`${detail.name}Comments`]:!!props.comments.length})
    }
    const showingComments=()=>{
        if(comp.state[`${detail.name}Comments`]){
            return(
                <FlatList
                    data={props.comments}
                    renderItem={({item})=>commentList(item,list)}
                    keyExtractor={(item,index)=>index.toString()}
                />
            )
        }
    }
    return (
        <>
            <View
                style={[styles.warning,styles.profileContainer,styles.baseFlatListSize,styles.feedOwner]}
            >   
                <View style={[styles.tenBottomMargin,styles.footer]}>
                    <Image source={{uri:`${config.development.server}/images/${detailUser.email}/${detailUser.displayPicture}`}}
                        style={[styles.dpMedium]}
                        resizeMode='cover'
                    />
                    <Text style={[styles.baseText,styles.link]}> {detailUser.firstName} {detailUser.lastName}</Text>
                </View>
                <Image source={{uri:`${config.development.server}/images/${props.email}/${props.name}`}}
                    style={[styles.feedImage,styles.minFeedWidth]}
                    resizeMode='cover'
                />
                <Text style={[styles.baseText,styles.link]}>{props.caption}</Text>
                <Text style={[styles.baseText]}><Ionicons name='ios-thumbs-up' size={20} color='white'/> {props.likes.length} likes</Text>
            </View>
            <View
                style={[styles.oneHdrdWidth,styles.feedContainer,styles.footer]}
            >
                <View
                    style={[styles.footer,styles.centerAlignItem,styles.likeContainer]}
                >
                    <TouchableOpacity
                        disabled={props.likes.some(like=>like==user)||comp.state[`${detail.name}Clicked`]}
                        onPress={()=>{
                            comp.setState({[`${detail.name}Clicked`]:true})
                            comp.props.likeFeed(props.name,user)
                        }}
                        style={[styles.footer,styles.centerAlignItem,styles.centerJustify,styles.oneHdrdWidth]}
                    >
                        <Ionicons name='ios-thumbs-up' size={25} color={props.likes.some(like=>like==user)||comp.state[`${detail.name}Clicked`]?'blue':'white'}/>
                        <Text style={styles.baseText}> {props.likes.some(like=>like==user)?'Liked':'Like'}</Text>
                    </TouchableOpacity>
                </View>
                <View
                    style={[styles.footer,styles.centerAlignItem,styles.likeContainer,styles.commentContainer]}
                >
                    <TouchableOpacity
                        style={[styles.footer,styles.centerAlignItem,styles.centerJustify,styles.oneHdrdWidth]}
                        onPress={()=>{
                            comp.setState({[`${detail.name}Comments`]:!comp.state[`${detail.name}Comments`]})
                        }}
                    >
                        <FontAwesome name='comments-o' size={25} color={'white'}/>
                        <Text style={styles.baseText}> Comments</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View
                    style={[styles.feedContainer,styles.commentList]}
                >
                {showingComments()}
                <TextInput
                    style={styles.commentBar}
                    onChangeText={text=>{
                        comp.setState({[`${[detail.name]}NewComment`]:text})
                        if(!text){
                            comp[detail.name].setNativeProps({style:[styles.disabledButton]})
                        } else{
                            comp[detail.name].setNativeProps({style:[styles.touchable,styles.notWarning]})
                        }
                    }}
                    multiline={true}
                    placeholder='type your comment'
                    blurOnSubmit={true}
                    value={comp.state[`${[detail.name]}NewComment`]}
                />
                <TouchableOpacity
                    disabled={!comp.state[`${[detail.name]}NewComment`]}
                    ref={component=>comp[detail.name]=component}
                    style={[styles.textInput,styles.touchable,styles.disabledButton,styles.commentButton]}
                    onPress={()=>{
                        const result={
                            email:user,
                            detail:comp.state[`${[detail.name]}NewComment`]
                        }
                        comp.setState({[`${[detail.name]}NewComment`]:'',[`${detail.name}Comments`]:true})
                        comp[detail.name].setNativeProps({style:[styles.disabledButton]})
                        comp.props.commentFeed(props.name,result)
                    }}
                >
                    <Text style={[styles.baseText,styles.link]}>comment</Text>
                </TouchableOpacity>
            </View>
        </>
    )
}

export const friendList=(props,userDetail,follow,checkingFeed,navigate)=>{
    const availability=userDetail.friends.some(friend=>friend==props.email)
    const display=()=>{
        if(availability){
            return(
                <Text style={styles.baseText}>You Are Following {props.firstName} {props.lastName}</Text>
            )
        }
        return(
            <>
                <Text style={styles.baseText}>You're not following {props.firstName} {props.lastName}, Please	click on Follow	to start following {props.firstName} {props.lastName}</Text>
            </>
        )
    }
    return (
        <TouchableOpacity
            disabled={!availability}
            style={[styles.feedOwner,styles.oneHdrdWidth,styles.feedContainer,styles.twoBorder]}
            onPress={()=>{
                navigate('FriendFeed',props)}
            }
        >
            <View style={[styles.footer,styles.centerAlignItem]}>
                <Image source={{uri:`${config.development.server}/images/${props.email}/${props.displayPicture}`}}
                    style={styles.dpBig}
                    resizeMode='cover'
                />
                <View style={styles.threeLeftMargin}>
                    <Text style={[styles.baseText,styles.link]}>{props.firstName} {props.lastName}</Text>
                    <TouchableOpacity
                        disabled={availability}
                        onPress={()=>{
                            follow(userDetail.email,props.email)
                            checkingFeed([...userDetail.friends,userDetail.email,props.email])
                            navigate('Main')
                        }}
                        style={[styles.footer,styles.flexEndAlignItem]}
                    >
                        <Icon name={availability?'user-following':'user-follow'} size={25} color='white'/>
                        <Text style={[styles.baseText,styles.link]}> {availability?'Following':'Follow'}</Text>
                    </TouchableOpacity>
                </View>
            </View>
            {display()}
        </TouchableOpacity>
    )
}