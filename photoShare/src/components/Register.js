import {styles} from '../styles'
import React,{Component} from 'react'
import {View,Text,TextInput,TouchableOpacity,Image} from 'react-native'
import {connect} from 'react-redux'
import {loginUser,checkUniqueHandle} from '../actions'

class Register extends Component{
    static navigationOptions={
        header:null
    }
    constructor(props){
        super(props)
        this.state={
            email:'',
            password:'',
            reenterPassword:'',
            emailError:'Please input valid email',
            passwordError:'Please input password',
            repasswordError:'Please input match re-enter password',
            disabled:true,
            checking:false,
            checkingMessage:'Input the Email Id'
        }
    }
    displayEmail(condition){
        if(condition){
            return(<TextInput
                value={this.state.checkingMessage}
                multiline={true}
                style={this.state.checkingMessage.match('available')?[styles.textInput,styles.errorBox,styles.notErrorBox]:[styles.textInput,styles.errorBox]}
                editable={false}
            />)
        }
    }
    displayError(email,password,repassword){
        const error=email?
            (password?
                (repassword?
                    `${email}\n${password}\n${repassword}`
                    :`${email}\n${password}`
                )
                :(repassword?
                    `${email}\n${repassword}`
                    :email)
            ):(password?
                (repassword?
                    `${password}\n${repassword}`
                    :password)
                :(repassword?
                    repassword
                    :null)
            )
        if(error){
            if(this.button){
                this.button.setNativeProps({
                    style:styles.disabledButton
                })
            }
            if(!this.state.disabled){
                this.setState({disabled:true})
            }
            return(
                <TextInput
                    value={error}
                    multiline={true}
                    numberOfLines={3}
                    style={[styles.textInput,styles.errorBox]}
                    editable={false}
                />
            )
        } else{
            if(this.state.checkingMessage.match('available')){
                this.button.setNativeProps({
                    style:[styles.touchable,styles.notWarning]
                })
                if(this.state.disabled){
                    this.setState({disabled:false})
                }
            } else{
                this.button.setNativeProps({
                    style:styles.disabledButton
                })
                if(!this.state.disabled){
                    this.setState({disabled:true})
                }
            }
        }
    }
    render(){
        const {navigate}=this.props.navigation
        return(
            <View style={styles.baseBackground}>
                <View>
                    <Image source={require('../icons/logo.png')}/>
                    <TextInput
                        autoFocus={true}
                        ref={component=>this.email=component}
                        onFocus={()=>this.setState({checking:true})}
                        style={styles.textInput}
                        placeholder='Email Id'
                        keyboardType='email-address'
                        value={this.state.email}
                        onChangeText={text=>this.setState({email:text,checkingMessage:'Submit to check...'})}
                        onBlur={e=>{
                            const {email}=this.state
                            if(!email.match(/^\w+@\w+(.\w+)+$/)){
                                this.email.setNativeProps({style:styles.warning})
                                this.setState({emailError:'Invalid E-mail',checkingMessage:'Invalid E-mail'})
                            } else if(!email.match(/^\w+@realuniversity.(?:com||edu){1}$/)){
                                this.email.setNativeProps({style:styles.warning})
                                this.setState({emailError:'Invalid E-mail Domain',checkingMessage:'Invalid E-mail Domain'})
                            } else{
                                this.email.setNativeProps({style:styles.notWarning})
                                this.setState({emailError:null})
                                this.props.checkUniqueHandle(email)
                                    .then(res=>{
                                        this.setState({checkingMessage:res})
                                    })
                            }
                        }}
                        onSubmitEditing={()=>this.password.focus()}
                    />
                    {this.displayEmail(this.state.checking)}
                    <TextInput
                        ref={component=>this.password=component}
                        style={styles.textInput}
                        placeholder='Password'
                        secureTextEntry={true}
                        value={this.state.password}
                        onChangeText={text=>this.setState({password:text,error:null})}
                        onBlur={e=>{
                            if(!this.state.password){
                                this.password.setNativeProps({style:styles.warning})
                                this.setState({passwordError:'Please input the password'})
                            } else{
                                if(this.state.reenterPassword!=this.state.password){
                                    this.reenterPassword.setNativeProps({style:styles.warning})
                                    this.setState({repasswordError:'Password MissMatch'})
                                } else{
                                    this.reenterPassword.setNativeProps({style:styles.notWarning})
                                    this.setState({repasswordError:null})
                                }
                                this.setState({passwordError:null})
                                this.password.setNativeProps({style:styles.notWarning})
                            }
                        }}
                        onSubmitEditing={()=>this.reenterPassword.focus()}
                    />
                    <TextInput
                        ref={component=>this.reenterPassword=component}
                        style={styles.textInput}
                        placeholder='Re-enter Password'
                        secureTextEntry={true}
                        value={this.state.reenterPassword}
                        onChangeText={text=>this.setState({reenterPassword:text,error:null})}
                        onBlur={e=>{
                            const {password}=this.state
                            const {reenterPassword}=this.state
                            if(reenterPassword!=password){
                                this.reenterPassword.setNativeProps({style:styles.warning})
                                this.setState({repasswordError:'Password MissMatch'})
                            } else {
                                this.setState({repasswordError:null})
                                this.reenterPassword.setNativeProps({style:styles.notWarning})
                            }
                        }}
                    />
                    <TouchableOpacity
                        ref={component=>this.button=component}
                        style={[styles.textInput,styles.touchable,styles.disabledButton]}
                        disabled={this.state.disabled}
                        onPress={()=>{
                            this.props.loginUser('create',{
                                email:this.state.email,
                                password:this.state.password
                            }).then(res=>{
                                if(res.message){
                                    this.setState({emailError:res.message})
                                } else{
                                    navigate('ProfileCreation',res)
                                }
                            })
                        }}
                    >
                        <Text
                            style={[styles.baseText,styles.buttonText,styles.link]}
                        >Register</Text>
                    </TouchableOpacity>
                    {this.displayError(this.state.emailError,this.state.passwordError,this.state.repasswordError)}
                </View>
                <View style={styles.footer}>
                        <Text style={styles.baseText}>Already have an account? </Text>
                        <Text
                            style={[styles.baseText,styles.link]}
                            onPress={()=>navigate('Login')}>Login</Text>
                 </View>
            </View>
        )
    }
}

const mapStateToProps=state=>{
    return {
        login:state.login
    }
}

export default connect(
    mapStateToProps,
    {loginUser,checkUniqueHandle}
)(Register)