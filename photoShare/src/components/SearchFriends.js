import React,{Component} from 'react'
import {View,Text,Image,TouchableOpacity,FlatList,ScrollView,TextInput} from 'react-native'
import {connect} from 'react-redux'
import Icon from 'react-native-vector-icons/Ionicons'
import {styles} from '../styles'
import {followFriends,checkFeed} from '../actions'
import {List,friendList} from './ListComponent'

class SearchFriends extends Component {
    static navigationOptions={
        header:null
    }
    constructor(props){
        super(props)
        this.state={}
    }
    display(){
        return(
            <>
                <FlatList
                    data={
                        this.state.friends?
                        this.props.login.userList
                            .filter(user=>user.email!=this.props.login.userDetail.email)
                            .filter(user=>user.firstName.match(this.state.friends)||user.lastName.match(this.state.friends))
                            .sort((a,b)=>{
                                if(`${a.firstName} ${a.lastName}`<`${b.firstName} ${b.lastName}`){
                                    return 1
                                }
                                return 0
                            })
                        :[]
                    }
                    renderItem={({item})=>friendList(item,
                                                    this.props.login.userDetail,
                                                    this.props.followFriends,
                                                    this.props.checkFeed,
                                                    this.props.navigation.navigate)}
                    keyExtractor={(item,index)=>index.toString()}
                    style={[styles.oneHdrdWidth,styles.friendList]}
                />
            </>
        )   
    }
    render(){
        return(
            <View style={[styles.baseBackground,styles.mainContainer]}>
                <View style={[styles.textInput,styles.footer,styles.centerAlignItem,styles.oneHdrdWidth]}>
                    <TextInput
                        style={[styles.oneHdrdWidth,styles.searchBar]}
                        placeholder='Search a friend'
                        maxLength={20}
                        onChangeText={text=>this.setState({friends:text})}
                        autoFocus={true}
                    />
                    <Icon size={25} name='ios-search' color='red' style={[styles.headerBar,styles.searchIcon]}/>
                </View>
                {this.display()}
            </View>
        )
    }
}

const mapStateToProps=state=>{
    return{
        login:state.login,
        feed:state.feed
    }
}

export default connect(
    mapStateToProps,
    {followFriends,checkFeed}
)(SearchFriends)