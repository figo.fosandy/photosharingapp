import React,{Component} from 'react'
import {View,Text,Image,TextInput,TouchableOpacity,Alert} from 'react-native'
import {styles} from '../styles'
import {connect} from 'react-redux'
import {uploadImage,updateProfile,logout,checkFeed,getUserList} from '../actions'
import ImagePicker from 'react-native-image-picker'
import path from 'path'
import server from '../../config.json'
import Icon from 'react-native-vector-icons/Ionicons'

class Profile extends Component {
    constructor(props){
        super(props)
        this.state={
        }
    }
    render(){
        return(
            <View style={[styles.baseBackground]}>
                <TouchableOpacity
                    style={[styles.headerBar,styles.searchIcon,styles.logoutIcon]}
                    onPress={()=>{
                        Alert.alert(
                            'Logout',
                            'Are you sure?',
                             [
                              {text: 'Cancel', onPress: () =>{
                                  this.props.navigation.navigate('Main')
                              }, style: 'cancel'},
                              {text: 'OK', onPress: () =>{
                                  this.props.navigation.navigate('Auth')
                                  this.props.logout()
                              }},
                            ]
                          )
                     
                    }}
                >
                    <Icon name='ios-log-out' color='white' size={25}/>
                </TouchableOpacity>
                <Image source={this.state.avatarSource?this.state.avatarSource
                        :(this.props.login.userDetail
                            ?(this.props.login.userDetail.displayPicture
                                ?{uri:`${server.development.server}/images/${this.props.login.userDetail.email}/${this.props.login.userDetail.displayPicture}`}
                                :require('../icons/profile.png'))
                            :require('../icons/profile.png'))}
                    style={styles.avatarSize}
                />
                <TouchableOpacity
                    onPress={()=>{
                        const options = {
                            title: 'Select Avatar',
                            storageOptions: {
                              skipBackup: true,
                              path: 'images',
                            },
                          };
                          ImagePicker.showImagePicker(options, (response) => {
                            if (response.didCancel) {
                              console.log('User cancelled image picker');
                            } else if (response.error) {
                              console.log('ImagePicker Error: ', response.error);
                            } else if (response.customButton) {
                              console.log('User tapped custom button: ', response.customButton);
                            } else {
                              const source = { uri: response.uri };
                              this.setState({avatarSource:source,data:response.data,avatarFileName:response.fileName})
                              this.button.setNativeProps({style:styles.touchable})
                              this.discard.setNativeProps({style:styles.touchable})
                            }
                          })
                    }}
                    style={[styles.textInput,styles.touchable,styles.imageButton]}
                >
                    <Text
                        style={[styles.baseText,styles.buttonText,styles.link]}
                    >Change DP</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.textInput,styles.touchable,styles.disabledButton,styles.centerAlignItem,styles.profileContainer]}
                    disabled={true}
                >
                    <View style={[styles.footer,styles.centerAlignItem]}>
                        <Text style={[styles.baseText,styles.profileLeftSide]}>Unique Handle</Text>
                        <Text style={[styles.baseText,styles.profileRightSide]}>: {this.props.login.userDetail.uniqueHandle||''}</Text>
                    </View>
                    <View style={[styles.footer,styles.centerAlignItem]}>
                        <Text style={[styles.baseText,styles.profileLeftSide]}>First Name</Text>
                        <Text style={[styles.baseText,styles.profileRightSide]}>: {this.props.login.userDetail.firstName||''}</Text>
                    </View>
                    <View style={[styles.footer,styles.centerAlignItem]}>
                        <Text style={[styles.baseText,styles.profileLeftSide]}>Last Name</Text>
                        <Text style={[styles.baseText,styles.profileRightSide]}>: {this.props.login.userDetail.lastName||''}</Text>
                    </View>
                    <View style={[styles.footer,styles.centerAlignItem]}>
                        <Text style={[styles.baseText,styles.profileLeftSide,styles.profileLeftSide]}>Registered E-mail</Text>
                        <Text style={[styles.baseText,styles.profileRightSide,styles.profileEmailRight]}>: {this.props.login.userDetail.email||''}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    ref={component=>this.button=component}
                    style={[styles.textInput,styles.touchable,styles.disabledButton]}
                    disabled={!this.state.avatarSource}
                    onPress={()=>{
                        const {uri}=this.state.avatarSource
                        const image=this.state.avatarFileName
                        const imageDetail=path.parse(image)
                        const filename=`${imageDetail.name}${Date.now()}${imageDetail.ext}`
                        const payload={displayPicture:filename}
                        this.props.uploadImage(this.props.login.userDetail.email,uri,filename,'Update Display Picture','yes')
                            .then(()=>this.props.updateProfile(this.props.login.userDetail.email,payload))
                            .then(()=>{
                                this.setState({avatarSource:null})
                                this.props.navigation.navigate('Main')
                                this.props.getUserList()
                                this.discard.setNativeProps({style:styles.disabledButton})
                                this.button.setNativeProps({style:styles.disabledButton})
                            })
                    }}
                >
                    <Text
                        style={[styles.baseText,styles.buttonText,styles.link]}
                    >Save Profile</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    ref={component=>this.discard=component}
                    style={[styles.textInput,styles.touchable,styles.disabledButton]}
                    disabled={!this.state.avatarSource}
                    onPress={()=>{
                        this.setState({avatarSource:null})
                        this.props.navigation.navigate('Main')
                        this.discard.setNativeProps({style:styles.disabledButton})
                        this.discard.setNativeProps({style:styles.disabledButton})
                    }}
                >
                    <Text
                        style={[styles.baseText,styles.buttonText,styles.link]}
                    >Discard Changes</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const mapStateToProps=state=>{
    return {
        login:state.login,
        feed:state.feed
    }
}

export default connect(
    mapStateToProps,
    {updateProfile,uploadImage,logout,checkFeed,getUserList}
)(Profile)