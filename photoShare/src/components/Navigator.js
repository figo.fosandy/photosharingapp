import {createAppContainer,createSwitchNavigator} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import Login from './Login'
import Register from './Register'
import Main from './Main'
import CreateNewFeed from './CreateNewFeed'
import ProfileCreation from './ProfileCreation'
import AuthLoadingScreen from './AuthLoadingScreen'
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs'
import Icon from 'react-native-vector-icons/Ionicons'
import React from 'react'
import {View} from 'react-native'
import SearchFriends from './SearchFriends'
import Profile from './Profile'
import FriendFeed from './FriendFeed'

const AppStack=createMaterialBottomTabNavigator({
    Main:{
        screen:Main,
        navigationOptions:{
            tabBarLabel:'Feed',
            tabBarIcon:tintColor=>(
                <View>
                    <Icon size={25} name={'ios-home'} color={tintColor}/>
                </View>
            )
        }
    },
    SearchFriends:{
        screen:SearchFriends,
        navigationOptions:{
            tabBarLabel:'Search',
            tabBarIcon:tintColor=>(
                <View>
                    <Icon size={25} name={'ios-search'} color={tintColor}/>
                </View>
            )
        }
    },
    Profile:{
        screen:Profile,
        navigationOptions:{
            tabBarLabel:'Profile',
            tabBarIcon:tintColor=>(
                <View>
                    <Icon size={25} name={'ios-person'} color={tintColor}/>
                </View>
            )
        }
    }
},{defaultNavigationOptions:{
    activeColor: 'white',
    inactiveColor: 'black'
}})

const AuthStack=createStackNavigator({
    Login:Login,
    Register:Register,
    ProfileCreation:ProfileCreation,
    CreateNewFeed:CreateNewFeed
})

const Navigator=createSwitchNavigator(
    {
        AuthLoading:AuthLoadingScreen,
        App:AppStack,
        Auth:AuthStack,
        FriendFeed:FriendFeed
    },
    {
        initialRouteName:'AuthLoading'
    }
)

export default createAppContainer(Navigator)