import React,{Component} from 'react'
import {
    ActivityIndicator,
    StatusBar,
    View,
    Text,
    Image
} from 'react-native'
import {styles} from '../styles'
import {connect} from 'react-redux'

class AuthLoadingScreen extends Component{
    constructor(props){
        super(props)
    }
    componentDidMount(){
        const {login}=this.props
        this.props.navigation.navigate(login.isLogin?(login.userDetail.firstName?'App':'ProfileCreation'):'Auth')
    }
    render(){
        return(
            <View style={styles.baseBackground}>
                <StatusBar barStyle='dark-content'/>
                <Image source={require('../icons/logo.png')}/>
                <Text style={[styles.baseText,styles.link]}>Welcome To Photo Sharing App</Text>
                <ActivityIndicator/>
            </View>
        )
    }
}

const mapStateToProps=state=>{
    return{
        login:state.login
    }
}

export default connect(
    mapStateToProps,
    {}
)(AuthLoadingScreen)