import React,{Component} from 'react'
import {View,Text,Image,TouchableOpacity,TextInput} from 'react-native'
import {connect} from 'react-redux'
import {styles} from '../styles'
import {checkFeed,uploadImage} from '../actions'
import path from 'path'

class CreateNewFeed extends Component {
    static navigationOptions={
        header:null
    }
    constructor(props){
        super(props)
        const {getParam}=this.props.navigation
        this.state={
            data:getParam('data')||null,
            avatarSource:getParam('avatarSource')||null,
            caption:'',
            error:null
        }
    }
    display(){
        const {getParam}=this.props.navigation
        return(<>
            <Image
                source={this.state.avatarSource}
                style={[styles.oneHdrdWidth,styles.feedImage]}
                resizeMode='cover'
            />
            <TextInput
                autoFocus={true}
                ref={component=>this.caption=component}
                style={[styles.textInput]}
                placeholder='Caption'
                onChangeText={text=>{
                    this.setState({caption:text,error:null})
                    this.caption.setNativeProps({style:styles.notWarning})
                }}
                onSubmitEditing={()=>this.upload.props.onPress()}
                value={this.state.caption}
            />
            <TouchableOpacity
                ref={component=>this.upload=component}
                onPress={()=>{
                    if(!this.state.caption){
                        this.setState({
                            error:'Please add caption'
                        })
                        this.caption.setNativeProps({style:styles.warning})
                    } else{
                        const uri=this.state.avatarSource.uri
                        const uriDetail=path.parse(getParam('fileName'))
                        console.log(getParam('fileName'))
                        const filename=`${uriDetail.name}${Date.now()}${uriDetail.ext}`
                        this.props.uploadImage(this.props.login.userDetail.email,uri,filename,this.state.caption,'no')
                            .then(()=>{
                                const {userDetail}=this.props.login
                                this.props.checkFeed([...userDetail.friends,userDetail.email])
                                this.setState({avatarSource:null,caption:''})
                                this.props.navigation.setParams({avatarSource:null})
                                this.props.navigation.navigate('Main')
                            })
                    }
                }}
                style={[styles.textInput,styles.touchable,styles.imageButton]}
            >
                <Text
                    style={[styles.baseText,styles.buttonText,styles.link]}
                >Upload</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={()=>{
                    this.setState({avatarSource:null,caption:''})
                    this.props.navigation.setParams({avatarSource:null})
                    this.props.navigation.navigate('Main')
                }}
                style={[styles.textInput,styles.touchable,styles.imageButton]}
            >
                <Text
                    style={[styles.baseText,styles.buttonText,styles.link]}
                >Discard</Text>
            </TouchableOpacity>
            <TextInput
                value={this.state.error}
                multiline={true}
                style={[styles.textInput,styles.errorBox]}
                editable={false}
            />
        </>)
    }
    render(){
        return(
            <View style={[styles.baseBackground,styles.mainContainer]}>
                <View style={styles.headerBar}>
                    <Text
                        style={[styles.baseText,styles.link,styles.headerText]}
                    >Creating New Feed</Text>
                </View>
                <View style={[styles.oneHdrdWidth,styles.fiftyMarginTop]}>
                    {this.display()}
                </View>
            </View>
        )
    }
}

const mapStateToProps=state=>{
    return{
        login:state.login,
        feed:state.feed
    }
}

export default connect(
    mapStateToProps,
    {uploadImage,checkFeed}
)(CreateNewFeed)