import React,{Component} from 'react'
import {View,Text,Image,TextInput,TouchableOpacity} from 'react-native'
import {styles} from '../styles'
import {connect} from 'react-redux'
import {uploadImage,updateProfile,checkUniqueHandle} from '../actions'
import ImagePicker from 'react-native-image-picker'
import path from 'path'

class ProfileCreation extends Component {
    static navigationOptions={
        header:null
    }
    constructor(props){
        super(props)
        this.state={
            email:this.props.navigation.getParam('email')||this.props.login.userDetail.email,
            detailUser:{
                firstName:'',
                lastName:'',
                uniqueHandle:''
            },
            checkingHandle:'Input The Unique Handle',
            checking:false,
            disabled:true,
            firstNameError:'Please input the First Name',
            lastNameError:'Please input the Last Name',
            uniqueHandleError:'Please input the Unique Handle'
        }
    }
    displayUniqueHandle(condition){
        if(condition){
            return(<TextInput
                value={this.state.checkingHandle}
                multiline={true}
                style={this.state.checkingHandle.match('available')?[styles.textInput,styles.errorBox,styles.notErrorBox]:[styles.textInput,styles.errorBox]}
                editable={false}
            />)
        }
    }
    displayError(firstName,lastName,uniqueHandle){
        const error=firstName?
            (lastName?
                (uniqueHandle?
                    `${firstName}\n${lastName}\n${uniqueHandle}`
                    :`${firstName}\n${lastName}`
                )
                :(uniqueHandle?
                    `${firstName}\n${uniqueHandle}`
                    :firstName)
            ):(lastName?
                (uniqueHandle?
                    `${lastName}\n${uniqueHandle}`
                    :lastName)
                :(uniqueHandle?
                    uniqueHandle
                    :null)
            )
        if(error){
            if(this.button){
                this.button.setNativeProps({
                    style:styles.disabledButton
                })
            }
            if(!this.state.disabled){
                this.setState({disabled:true})
            }
            return(
                <TextInput
                    value={error}
                    multiline={true}
                    numberOfLines={3}
                    style={[styles.textInput,styles.errorBox]}
                    editable={false}
                />
            )
        } else{
            if(this.state.checkingHandle.match('available')){
                this.button.setNativeProps({
                    style:[styles.touchable,styles.notWarning]
                })
                if(this.state.disabled){
                    this.setState({disabled:false})
                }
            } else{
                this.button.setNativeProps({
                    style:styles.disabledButton
                })
                if(!this.state.disabled){
                    this.setState({disabled:true})
                }
            }
        }
    }
    render(){
        return(
            <View style={styles.baseBackground}>
                <Image source={this.state.avatarSource?this.state.avatarSource:require('../icons/profile.png')}
                    style={styles.avatarSize}
                />
                <TextInput
                    ref={component=>this.firstName=component}
                    maxLength={20}
                    style={[styles.textInput]}
                    placeholder='First Name'
                    autoFocus={true}
                    value={this.state.detailUser.firstName}
                    onChangeText={text=>this.setState({detailUser:{...this.state.detailUser,firstName:text}})}
                    onBlur={()=>{
                        const {firstName}=this.state.detailUser
                        if(!firstName){
                            this.setState({firstNameError:'Please input the First Name'})
                            this.firstName.setNativeProps({style:styles.warning})
                        } else {
                            if(!firstName.match(/[^a-zA-Z\s]/)){
                                this.setState({firstNameError:null})
                                this.firstName.setNativeProps({style:styles.notWarning})
                            } else{
                                this.setState({firstNameError:'Please input the valid First Name'})
                                this.firstName.setNativeProps({style:styles.warning})
                            }
                        }
                    }}
                    onSubmitEditing={()=>this.lastName.focus()}
                />
                <TextInput
                    ref={component=>this.lastName=component}
                    style={[styles.textInput]}
                    placeholder='Last Name'
                    maxLength={20}
                    onChangeText={text=>this.setState({detailUser:{...this.state.detailUser,lastName:text}})}
                    onBlur={()=>{
                        const {lastName}=this.state.detailUser
                        if(!lastName){
                            this.setState({lastNameError:'Please input the Last Name'})
                            this.lastName.setNativeProps({style:styles.warning})
                        } else {
                            if(!lastName.match(/[^a-zA-Z\s]/)){
                                this.setState({lastNameError:null})
                                this.lastName.setNativeProps({style:styles.notWarning})
                            } else{
                                this.setState({lastNameError:'Please input the valid Last Name'})
                                this.lastName.setNativeProps({style:styles.warning})
                            }
                        }
                    }}
                    onSubmitEditing={()=>this.uniqueHandle.focus()}
                />
                <TextInput
                    ref={component=>this.uniqueHandle=component}
                    style={[styles.textInput]}
                    placeholder='Unique Handle'
                    onFocus={()=>this.setState({checking:true})}
                    onChangeText={text=>this.setState({detailUser:{...this.state.detailUser,uniqueHandle:text},checkingHandle:'Submit to check..'})}
                    onBlur={()=>{
                        const {uniqueHandle}=this.state.detailUser
                        if(!uniqueHandle){
                            this.setState({uniqueHandleError:'Please input the Unique Handle'})
                            this.uniqueHandle.setNativeProps({style:styles.warning})
                        } else {
                            if(!uniqueHandle.match(/[^\w]/)){
                                this.setState({uniqueHandleError:null})
                                this.uniqueHandle.setNativeProps({style:styles.notWarning})
                                this.props.checkUniqueHandle(this.state.detailUser.uniqueHandle)
                                    .then(res=>{
                                        this.setState({checkingHandle:res})
                                })
                            } else{
                                this.setState({uniqueHandleError:'Please input the valid Unique Handle',checkingHandle:'Please input the valid Unique Handle'})
                                this.uniqueHandle.setNativeProps({style:styles.warning})
                            }
                        }
                    }}
                />
                {this.displayUniqueHandle(this.state.checking)}
                <TouchableOpacity
                    onPress={()=>{
                        const options = {
                            title: 'Select Avatar',
                            storageOptions: {
                              skipBackup: true,
                              path: 'images',
                            },
                          };
                          ImagePicker.showImagePicker(options, (response) => {
                            if (response.didCancel) {
                              console.log('User cancelled image picker');
                            } else if (response.error) {
                              console.log('ImagePicker Error: ', response.error);
                            } else if (response.customButton) {
                              console.log('User tapped custom button: ', response.customButton);
                            } else {
                              const source = { uri: response.uri };
                              this.setState({avatarSource:source,data:response.data,avatarFileName:response.fileName})
                            }
                          })
                    }}
                    style={[styles.textInput,styles.touchable,styles.imageButton]}
                >
                    <Text
                        style={[styles.baseText,styles.buttonText,styles.link]}
                    >Display Picture</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    ref={component=>this.button=component}
                    style={[styles.textInput,styles.touchable,styles.disabledButton]}
                    disabled={this.state.disabled}
                    onPress={()=>{
                        const uri=this.state.avatarSource?this.state.avatarSource.uri:null
                        const image=this.state.avatarFileName?this.state.avatarFileName:null
                        const imageDetail=image?path.parse(image):null
                        const filename=imageDetail?`${imageDetail.name}${Date.now()}${imageDetail.ext}`:null
                        const payload=filename?{...this.state.detailUser,displayPicture:filename}:this.state.detailUser
                        if(filename){
                            this.props.uploadImage(this.state.email,uri,filename,'Update Display Picture','yes')
                        }
                        this.props.updateProfile(this.state.email,payload)
                            .then(()=>this.props.navigation.navigate('Login'))
                    }}
                >
                    <Text
                        style={[styles.baseText,styles.buttonText,styles.link]}
                    >Save Profile</Text>
                </TouchableOpacity>
                {this.displayError(this.state.firstNameError,this.state.lastNameError,this.state.uniqueHandleError)}
            </View>
        )
    }
}

const mapStateToProps=state=>{
    return {
        login:state.login
    }
}

export default connect(
    mapStateToProps,
    {updateProfile,uploadImage,checkUniqueHandle}
)(ProfileCreation)