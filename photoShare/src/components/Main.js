import React,{Component} from 'react'
import {View,Text,Image,TouchableOpacity,FlatList,ScrollView} from 'react-native'
import {connect} from 'react-redux'
import Icon from 'react-native-vector-icons/Ionicons'
import {styles} from '../styles'
import {loginUser,checkFeed,getUserList,likeFeed,commentFeed} from '../actions'
import ImagePicker from 'react-native-image-picker'
import {List} from './ListComponent'

class Main extends Component {
    componentDidMount(){
        if(this.props.login.isLogin){
            this.props.getUserList()
            this.props.checkFeed([...this.props.login.userDetail.friends,this.props.login.userDetail.email])
        } else{
            this.props.getUserList()
            this.props.loginUser('check',{
                uniqueHandle:this.props.login.userDetail.uniqueHandle,
                password:this.props.login.userDetail.password
            }).then(()=>{
                this.props.checkFeed([...this.props.login.userDetail.friends,this.props.login.userDetail.email])
            })
        }
        this.setState({list:this.props.feed.list})
    }
    
    constructor(props){
        super(props)
        this.state={}
    }
    
    display(){
        if(this.props.login.loading){
            return(
                <Text style={styles.baseText}>Loading...</Text>
            )
        } else {
            if(!this.props.login.userDetail){
                return(
                    <Text style={styles.baseText}>Try to login...</Text>
                )
            }
            if(this.props.feed.loading){   
                return(
                    <>
                        <Text style={styles.baseText}>Loading Activity...</Text>
                    </>
                )
            }
            if(!this.props.feed.list.length){
                return(
                    <>
                        <Text style={styles.baseText}>No Activity on Feed, Please add a picture or friends</Text>
                        <TouchableOpacity
                            onPress={()=>{
                                const options = {
                                    title: 'Select Avatar',
                                    storageOptions: {
                                      skipBackup: true,
                                      path: 'images'
                                    }
                                  };
                                  ImagePicker.showImagePicker(options, (response) => {
                                    if (response.didCancel) {
                                      console.log('User cancelled image picker');
                                    } else if (response.error) {
                                      console.log('ImagePicker Error: ', response.error);
                                    } else if (response.customButton) {
                                      console.log('User tapped custom button: ', response.customButton);
                                    } else {
                                      const source = { uri: response.uri };
                                      this.props.navigation.navigate('CreateNewFeed',{
                                        avatarSource:source,
                                        data:response.data,
                                        fileName:response.fileName
                                      })
                                    }
                                  })
                            }}
                            style={[styles.textInput,styles.touchable,styles.imageButton,styles.footer,styles.uploadSize]}
                        >
                            <Text>Upload New Feed </Text>
                            <Icon name='ios-camera' size={30} color='#900'/>
                        </TouchableOpacity>
                    </>
                )
            }
            return(
                <>
                    <TouchableOpacity
                        onPress={()=>{
                            const options = {
                                title: 'Select Avatar',
                                storageOptions: {
                                    skipBackup: true,
                                    path: 'images'
                                }
                                };
                                ImagePicker.showImagePicker(options, (response) => {
                                if (response.didCancel) {
                                    console.log('User cancelled image picker');
                                } else if (response.error) {
                                    console.log('ImagePicker Error: ', response.error);
                                } else if (response.customButton) {
                                    console.log('User tapped custom button: ', response.customButton);
                                } else {
                                    const source = { uri: response.uri };
                                    this.props.navigation.navigate('CreateNewFeed',{
                                        avatarSource:source,
                                        data:response.data,
                                        fileName:response.fileName
                                    })
                                }
                                })
                        }}
                        style={[styles.textInput,styles.touchable,styles.imageButton,styles.footer,styles.uploadSize]}
                    >
                        <Text>Upload New Feed </Text>
                        <Icon name='ios-camera' size={30} color='#900'/>
                    </TouchableOpacity>
                    <FlatList
                        data={[
                            ...this.props.feed.list.filter(feed=>feed.likes.some(like=>like==this.props.login.userDetail.email)),
                            ...this.props.feed.list.filter(feed=>!feed.likes.some(like=>like==this.props.login.userDetail.email))
                        ]}
                        renderItem={({item})=>List(item,this)}
                        keyExtractor={(item,index)=>index.toString()}
                        style={styles.baseFlatListSize}
                    />
                </>
            )   
        }
    }
    render(){
        return(
            <View style={[styles.baseBackground,styles.mainContainer]}>
                <View style={styles.headerBar}>
                    <Text
                        style={[styles.baseText,styles.link,styles.headerText]}
                    >Hi, {this.props.navigation.getParam('firstName')||this.props.login.userDetail.firstName||''}</Text>
                </View>
                <ScrollView style={styles.bodyContainer}>
                    <View style={styles.oneHdrdWidth}>
                        {this.display()}
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps=state=>{
    return{
        login:state.login,
        feed:state.feed
    }
}

export default connect(
    mapStateToProps,
    {loginUser,checkFeed,getUserList,likeFeed,commentFeed}
)(Main)