import {styles} from '../styles'
import React,{Component} from 'react'
import {View,Text,TextInput,TouchableOpacity,Image} from 'react-native'
import {connect} from 'react-redux'
import {loginUser} from '../actions'

class Login extends Component{
    static navigationOptions={
        header:null
    }
    constructor(props){
        super(props)
        this.state={
            username:'',
            password:'',
            reenterPassword:'',
            error:null
        }
    }
    displayError(error){
        if(error){
            return(
                <TextInput
                    value={error.message}
                    style={[styles.textInput,styles.errorBox]}
                    editable={false}
                    multiline={true}
                />
            )
        }
    }
    render(){
        const {navigate}=this.props.navigation
        return(
            <View style={styles.baseBackground}>
                <View>
                    <Image source={require('../icons/logo.png')}/>
                    <TextInput
                        autoFocus={true}
                        style={styles.textInput}
                        placeholder='Email Id or Unique Handle'
                        keyboardType='email-address'
                        value={this.state.username}
                        onChangeText={text=>this.setState({username:text})}
                        onBlur={e=>{
                            const {username}=this.state
                            if(!username){
                                this.username.setNativeProps({style:styles.warning})
                                this.setState({error:{message:'Please input the email or unique handle'}})
                            } else{
                                this.username.setNativeProps({style:styles.notWarning})
                                this.setState({error:null})
                            }
                        }}
                        onSubmitEditing={()=>this.password.focus()}
                        ref={input=>this.username=input}
                    />
                    <TextInput
                        style={styles.textInput}
                        placeholder='Password'
                        secureTextEntry={true}
                        value={this.state.password}
                        onChangeText={text=>this.setState({password:text})}
                        onBlur={e=>{
                            if(!this.state.password){
                                this.password.setNativeProps({style:styles.warning})
                                this.setState({error:{message:'Please input the password'}})
                            } else{
                                this.password.setNativeProps({style:styles.notWarning})
                                this.setState({error:null})
                            }
                        }}
                        ref={input=>this.password=input}
                        onSubmitEditing={()=>this.button.props.onPress()}
                    />
                    <TouchableOpacity
                        ref={input=>this.button=input}
                        style={[styles.textInput,styles.touchable]}
                        onPress={()=>{
                            if(!this.state.error){
                                if(!this.state.username){
                                    this.username.setNativeProps({style:styles.warning})
                                    this.setState({error:{message:'Please input the email id or unique handle'}})
                                } else if(!(this.state.password)){
                                    this.password.setNativeProps({style:styles.warning})
                                    this.setState({error:{message:'Please input the password'}})
                                } else {
                                    if(this.state.username.match(/^\w+@realuniversity.(?:com||edu){1}$/)){
                                        this.props.loginUser('check',{
                                            email:this.state.username,
                                            password:this.state.password
                                        }).then(res=>{
                                            if(res.message){
                                                if(res.message.match('404')){
                                                    this.setState({error:{message:'Invalid email or password'}})
                                                } else {
                                                    this.setState({error:res})
                                                }
                                            } else{
                                                navigate('Main',res)
                                            }
                                        })
                                    } else if(this.state.username.match(/^\w+$/)){
                                        this.props.loginUser('check',{
                                            uniqueHandle:this.state.username,
                                            password:this.state.password
                                        }).then(res=>{
                                            if(res.message){
                                                if(res.message.match('404')){
                                                    this.setState({error:{message:'Invalid email or password'}})
                                                } else {
                                                    this.setState({error:res})
                                                }
                                            } else{
                                                navigate('Main',res)
                                            }
                                        })
                                    } else{
                                        this.username.setNativeProps({style:styles.warning})
                                        this.setState({error:{message:'Please input valid email'}})
                                    }
                                }
                            }
                        }}
                    >
                        <Text style={[styles.baseText,styles.buttonText,styles.link]}>Login</Text>
                    </TouchableOpacity>
                    {this.displayError(this.state.error)}
                    <View style={styles.footer}>
                        <Text style={styles.baseText}>Don't have an account? </Text>
                        <Text
                            style={[styles.baseText,styles.link]}
                            onPress={()=>navigate('Register')}
                        >Register</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps=state=>{
    return {
        login:state.login
    }
}

export default connect(
    mapStateToProps,
    {loginUser}
)(Login)