import React,{Component} from 'react'
import {View,Text,Image,TouchableOpacity,FlatList,ScrollView} from 'react-native'
import {connect} from 'react-redux'
import {styles} from '../styles'
import {List} from './ListComponent'
import Icon from 'react-native-vector-icons/Ionicons'
import {likeFeed,commentFeed} from '../actions'


class FriendFeed extends Component {
    constructor(props){
        super(props)
        this.state={}
    }

    display(){
        if(!this.props.feed.list.filter(feed=>feed.email==this.props.navigation.getParam('email'))){
            return(
                <>
                    <Text style={styles.baseText}>{this.props.navigation.getParam('firstName')} {this.props.navigation.getParam('firstName')} has no any feed on him/her activity</Text>
                </>
            )
        }
        return(
            <>
                <FlatList
                    data={[
                        ...this.props.feed.list.filter(feed=>feed.email==this.props.navigation.getParam('email')).filter(feed=>feed.likes.some(like=>like==this.props.navigation.getParam('email'))),
                        ...this.props.feed.list.filter(feed=>feed.email==this.props.navigation.getParam('email')).filter(feed=>!feed.likes.some(like=>like==this.props.navigation.getParam('email')))
                    ]}
                    renderItem={({item})=>List(item,this)}
                    keyExtractor={(item,index)=>index.toString()}
                    style={styles.baseFlatListSize}
                />
            </>
        )   
    }
    render(){
        return(
            <View style={[styles.baseBackground,styles.mainContainer]}>
                <View style={[styles.headerBar,styles.fiftyRight]}>
                    <Text
                        style={[styles.baseText,styles.link,styles.mediumHeaderText]}
                    >{this.props.navigation.getParam('firstName')} {this.props.navigation.getParam('lastName')}'s Feeds</Text>
                </View>
                <TouchableOpacity
                    style={[styles.headerBar,styles.backIcon]}
                    onPress={()=>this.props.navigation.navigate('Main')}
                >
                    <Icon name='ios-arrow-round-back' color='white' size={50}/>
                </TouchableOpacity>
                <ScrollView style={styles.bodyContainer}>
                    <View style={styles.oneHdrdWidth}>
                        {this.display()}
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps=state=>{
    return{
        login:state.login,
        feed:state.feed
    }
}

export default connect(
    mapStateToProps,
    {likeFeed,commentFeed}
)(FriendFeed)