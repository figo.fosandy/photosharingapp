const mongoose=require(`./mongooseConnection`)
const fs=require('fs')
const db=mongoose.connection
db.on(`error`,console.error.bind(console,`connection error`))
db.once(`open`,()=>console.log(`Connected`))

const User=mongoose.model(`User`,{
    email:String,
    password:String,
    firstName:String,
    lastName:String,
    displayPicture:String,
    uniqueHandle:String,
    friends:Array
})

const Image=mongoose.model(`Image`,{
    name:String,
    email:String,
    caption:String,
    timeStamp:Number,
    isDp:Boolean,
    comments:Array,
    likes:Array
})

const notFoundResponse={
    statusCode:404,
    error:`Not Found`,
    message:`Not found`
}

const badRequestResponse={
    statusCode:400,
    error:`Bad Request`,
    message:`Invalid request input`
}

const conflictResponse={
    statusCode:409,
    error:`Conflict`,
    message:`Conflict request input`
}

const rootHandler=(request,h)=>{
    return h.response(`This is a root route`).code(200)
}

const registerNewUserHandler=async(request,h)=>{
    const payload=request.payload
    const users=await User.aggregate([
        {
            $group:{
                _id:`email`,
                emails:{
                    $push:`$email`
                }
            }
        }
    ])
    if(users[0]){
        const emailAvailability=users[0].emails.some(email=>email==payload.email)
        if(emailAvailability){
            return h.response(conflictResponse).code(409)
        }
    }
    fs.mkdirSync(`./images/${payload.email}`)
    await User.create(payload)
    return h.response(payload).code(201)
}

const checkUserHandler=async(request,h)=>{
    const payload=request.payload
    const result=await User.findOne(payload).lean()
    if(!result){
        return h.response(notFoundResponse).code(404)
    }
    return h.response(result).code(202)
}

const checkingHandler=async(request,h)=>{
    const {checking}=request.params
    const filter=checking.match('@')?{email:checking}:{uniqueHandle:checking}
    const result=await User.findOne(filter).lean()
    if(!result){
        return h.response(`${filter.email?'Email':'Unique Handle'} is available`).code(200)
    } 
    return h.response(`${filter.email?'Email':'Unique Handle'} has been taken`).code(200)
}

const updateProfile=async(request,h)=>{
    const {payload,params}=request
    const users=await User.aggregate([
        {
            $group:{
                _id:'uniqueHandles',
                uniqueHandles:{
                    $push:'$uniqueHandle'
                }
            }
        }
    ])
    if(users[0]){
        const handleAvailability=users[0].uniqueHandles.some(handle=>handle==payload.uniqueHandle)
        if(handleAvailability){
            return h.response(conflictResponse).code(409)
        }
    }
    const updatedProfile=await User.findOneAndUpdate({email:params.email},payload,{new:true})
    if(!updatedProfile){
        return h.response(notFoundResponse).code(404)
    }
    return h.response(updatedProfile).code(202)
}

const uploadImageHandler=async(request,h)=>{
    const {dir,file,caption,isDp}=request.params
    const rs=fs.createWriteStream(`./images/${dir}/${file}`);
    (request.payload.image).pipe(rs)
    await Image.create({
        name:file,
        email:dir,
        caption,
        timeStamp:Date.now(),
        isDp:isDp=='yes'?true:false
    })
    return h.response(file).code(201)
}

const findFeedHandler=async(request,h)=>{
    const result=await Image.find({
        email:{$in:request.params.emails.slice(1).slice(0,-1).split(',')},
        isDp:false
    })
        .sort({timeStamp:-1})
    return h.response(result).code(200)
}

const getAllUserHandler=async(request,h)=>{
    const result=await User.find({},{_id:1,email:1,uniqueHandle:1,displayPicture:1,firstName:1,lastName:1}).lean()
    return h.response(result).code(200)
}

const followFriendHandler=async(request,h)=>{
    const {follower,toBeFollow}=request.payload
    const result=await User.findOneAndUpdate({email:follower},{$push:{friends:toBeFollow}},{new:true}).lean()
    return h.response(result).code(202)
}

const likeFeedHandler=async(request,h)=>{
    const {name,email}=request.payload
    const result=await Image.findOneAndUpdate({name},{$push:{likes:email}},{new:true}).lean()
    return h.response(result).code(202)
}

const commentFeedHandler=async(request,h)=>{
    const {name,comment}=request.payload
    const result=await Image.findOneAndUpdate({name},{$push:{comments:comment}},{new:true}).lean()
    return h.response(result).code(202)
}

module.exports={rootHandler,registerNewUserHandler,checkUserHandler,uploadImageHandler,updateProfile,checkingHandler,findFeedHandler,getAllUserHandler,followFriendHandler,likeFeedHandler,commentFeedHandler}