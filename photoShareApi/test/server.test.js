'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it, after } = exports.lab = Lab.script();
const {start} = require('../server.js');
const btoa=require('btoa')
const FormData=require('form-data')
const fs=require('fs')
const streamToPromise=require('stream-to-promise')
const request=require('request')

const auth={
    Authorization:`Basic ${btoa('user:password')}`
}

describe('GET /', () => {
    let server;

    beforeEach(async () => {
        server = await start();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/'
        });
        expect(res.statusCode).to.equal(200)
        expect(res.payload).to.be.a.string()
        expect(res.payload).to.include('root')
    });
});

describe('PHOTO SHARING API', () => {
    const now=Date.now()
    let server;

    beforeEach(async () => {
        server = await start();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('responds when create new user account', async () => {
        const res = await server.inject({
            method: 'post',
            url: '/user/create',
            headers:auth,
            payload:{
                email:`user${now}@realuniversity.com`,
                password:'password'
            }
        });
        expect(res.statusCode).to.equal(201)
        expect(res.result).to.be.a.object()
        expect(res.result).to.include(['email','password'])
        expect(res.result.email).to.include('user')
        expect(res.result.password).to.equal('password')
    });

    it('checking user account', async () => {
        const res = await server.inject({
            method: 'post',
            url: '/user/check',
            headers:auth,
            payload:{
                email:`user${now}@realuniversity.com`,
                password:'password'
            }
        });
        expect(res.statusCode).to.equal(202)
        expect(res.result).to.be.a.object()
        expect(res.result).to.include(['email','password'])
        expect(res.result.email).to.include('user')
        expect(res.result.password).to.equal('password')
    });

    // it('updating user profile', async () => {
    //     const form=new FormData()
    //     await form.append('image',request('http://nodejs.org/images/logo.png'))
    //     const res=await server.inject({
    //         method: 'post',
    //         url: `/user/image/user${now}@realuniversity.com/${now}.png/caption/yes`,
    //         headers:auth,
    //         payload:form
    //     })
    //     expect(res.statusCode).to.equal(201)
    //     expect(res.result).to.be.a.string()
    //     expect(res.result).to.equal(now)
    // });
});