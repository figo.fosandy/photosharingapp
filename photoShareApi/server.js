const Hapi=require('@hapi/hapi')
const Joi=require('@hapi/joi')
const Bcrypt=require('bcrypt')
const qs=require('qs')
const {checkUserHandler,registerNewUserHandler,rootHandler,updateProfile,uploadImageHandler,checkingHandler,findFeedHandler,getAllUserHandler,followFriendHandler,likeFeedHandler,commentFeedHandler}=require('./handler')

const users={
    user:{
        username:'user',
        password:'$2a$04$BB3BAXMwyqkRhPjs7kUgXuWn7.pYxNXHyyDzD3fDo5xACgF2KFsCi',
        name:'Just User',
        id:'123456'
    }
}

const validate=async(request,username,password)=>{
    const user=users[username]
    if(!user){
        return {credentials:null,isValid:false}
    }
    const isValid=await Bcrypt.compare(password, user.password)
    const credentials={id:user.id,name:user.name}
    return {isValid,credentials}
}

const getDate=(date)=>{
    const thisDate=new Date(date)
    return thisDate.toISOString().slice(0,10)
}

const getDateLog=(date)=>{
    return `logs/${getDate(date)}.log`
}

const start=async()=>{
    const server=Hapi.server({
        port:2709, ///can come from config
        host:'0.0.0.0',
        query:{
            parser:(query)=>qs.parse(query)
        }
    })

    await server.register([require('@hapi/basic'),require('@hapi/inert')])

    await server.register({
        plugin:require('hapi-pino'),
        options:{
            prettyPrint:process.env.NODE_ENV!=='production',
            redact:['req.headers.authorization'],
            stream:getDateLog(Date.now())
        }
    })

    server.auth.strategy('simple','basic',{validate})

    server.route({
        method:'GET',
        path:'/',
        handler:rootHandler
    })
    
    server.route({
        method:'POST',
        path:'/user/create',
        handler:registerNewUserHandler,
        options:{
            auth:'simple',
            validate:{
                payload:{
                    email:Joi.string().email().required(),
                    password:Joi.string().required()
                }
            }
        }
    })

    server.route({
        method:'POST',
        path:'/user/check',
        handler:checkUserHandler,
        options:{
            auth:'simple',
            validate:{
                payload:{
                    email:Joi.string().email(),
                    uniqueHandle:Joi.string(),
                    password:Joi.string().required()
                }
            }
        }
    })
    
    server.route({
        method:'GET',
        path:'/user/handle/{checking}',
        handler:checkingHandler,
        options:{
            auth:'simple',
            validate:{
                params:{
                    checking:Joi.string()
                }
            }
        }
    })

    server.route({
        method:'POST',
        path:'/user/image/{dir}/{file}/{caption}/{isDp}',
        handler:uploadImageHandler,
        config:{
            payload:{
                maxBytes:209715200,
                output:'stream',
                parse:true
            }
        }
    })

    server.route({
        method:'PUT',
        path:'/user/profile/{email}',
        handler:updateProfile,
        options:{
            auth:'simple',
            validate:{
                params:{
                    email:Joi.string()
                },
                payload:{
                    firstName:Joi.string(),
                    lastName:Joi.string(),
                    displayPicture:Joi.string(),
                    uniqueHandle:Joi.string()
                }
            }
        }
    })

    server.route({
        method:'GET',
        path:'/images/{file*}',
        handler:{
            directory:{
                path:'./images',
                listing:true
            }
        }
    })

    server.route({
        method:'GET',
        path:'/user/feed/{emails}',
        handler:findFeedHandler,
        options:{
            auth:'simple'
        }
    })

    server.route({
        method:'GET',
        path:'/user/all',
        handler:getAllUserHandler,
        options:{
            auth:'simple'
        }
    })

    server.route({
        method:'PATCH',
        path:'/user/follow',
        handler:followFriendHandler,
        options:{
            auth:'simple'
        }
    })

    server.route({
        method:'PATCH',
        path:'/feed/like',
        handler:likeFeedHandler,
        options:{
            auth:'simple'
        }
    })

    server.route({
        method:'PATCH',
        path:'/feed/comment',
        handler:commentFeedHandler,
        options:{
            auth:'simple'
        }
    })

    await server.start()

    process.on('unhandledRejection',(err)=>{
        console.log(err)
        process.exit(1)
    })
    
    return server
}

module.exports={start}